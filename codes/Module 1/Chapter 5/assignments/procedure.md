## Procedure :
 * ### Example
 0.3x<sub>1</sub> - 0.2x<sub>2</sub> + 10x<sub>3</sub> = 71.4  
 3x<sub>1</sub> - 0.1x<sub>2</sub> - 0.2x<sub>3</sub> = 7.85  
 0.1x<sub>1</sub> + 7x<sub>2</sub> - 0.3x<sub>3</sub> =- 19.3  
  
1. Select no of equation 3 and Enter the data in the following format.  
2. Check Do pivoting and hit solve.    
3. Perform the solve again by unchecking Do pivoting and observe the result.  

![image004](image004.jpg )


